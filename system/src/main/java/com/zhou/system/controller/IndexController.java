package com.zhou.system.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhouben
 * @version 1.0
 * @date 17:12 2023/12/1
 * @description
 **/
@RestController
@RequestMapping("system")
public class IndexController {

    @GetMapping
    public String index() {
        return "system";
    }
}
